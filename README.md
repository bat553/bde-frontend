BDE RT Grenoble
---


> Baptiste Pellarin <swano@swano-lab.net> 2019
> 
> BDE RT Grenoble <iut1.rt.bde@univ-grenoble-alpes.fr> 2019

[License Apache 2](LICENSE.md)

## Cibles 

1. Les étudiants de RT Grenoble
2. Les partenaires
3. Les étudiants de l'UGA

## Fonctions principales

* Afficher 
  + Les évènements organisés par le BDE
    * Lien vers l'évènement Facebook 
    * Voter pour la playlist de la soirée
    * Informations sur le lieu
      - Ouverture 
      - Prix entrée 
      - La conso' recomandée par le BDE
  + Les informations pratiques
    * Aide pour les 1ère années
    * Les membres du BDE
  + Les participants au système de parrainage 
  + Les liens vers les réseaux sociaux du BDE
  + Les informations sur la carte emblem
    + Date des ventes 
    + Avantages 
    + Lien vers le site officiel
  + Liens vers nos partenaires
  + Les news
  + La vie en RT 
  
## Fonctionnement

1. Le site est compilé du `typescript` en `javascript` 
2. Le site est envoyé sur un système de stockage statique (`S3`)
3. L'utilisateur va envoyer des requêtes vers l'API.
4. Les réponses (`JSON`) sont ensuite mises en forme par le `javascript`

**L'API doit être programmé pour accepter les requêtes provenant de l'extérieur.**


