/*
 *
 * bde-frontend
 * summary-lieu.component.ts
 *
 * Created by Swano 6/25/19 11:52 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-summary-lieu',
  templateUrl: './summary-lieu.component.html',
  styleUrls: ['./summary-lieu.component.css']
})
export class SummaryLieuComponent implements OnInit {


  id_lieu : string;
  lieu_infos : {};
  next_events = [];
  past_events = [];
  constructor(private route: ActivatedRoute, public Auth: AuthService) { }

  ngOnInit() {
    this.Auth.reset_error();
    this.id_lieu = this.route.snapshot.paramMap.get('id');
    if (this.route.snapshot.paramMap.get('id')){
      this.Auth.get_a_lieu_events(this.route.snapshot.paramMap.get('id'))
        .subscribe(reply =>{
          this.lieu_infos = reply['lieu'];
          this.Auth.set_title(reply['lieu']['nom']);
          this.Auth.set_meta('description', reply['lieu']['nom']);
          let now = new Date().getTime();
          reply['events'].forEach((event)=>{
            if (new Date(event['date']).getTime() > now){
              this.next_events.push(event)
            } else {
              this.past_events.push(event)
            }
          });
        }, error1 => {
          this.Auth.error_handler(error1)
        })
    }
  }

}
