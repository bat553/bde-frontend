/*
 *
 * bde-frontend
 * reseaux.component.ts
 *
 * Created by Swano 28/07/19 19:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-reseaux',
  templateUrl: './reseaux.component.html',
  styleUrls: ['./reseaux.component.scss']
})
export class ReseauxComponent implements OnInit {
  @Input() only: boolean;
  constructor(public Auth: AuthService) { }

  ngOnInit() {
  }

}
