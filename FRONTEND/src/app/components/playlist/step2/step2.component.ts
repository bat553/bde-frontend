/*
 *
 * bde-frontend
 * step2.component.ts
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit, OnDestroy } from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  done: boolean;
  loading: boolean;

  constructor(public Auth: AuthService) {
    if (!this.Auth.playlist_selected.length){
      // Retour étape 1
      this.Auth.router_step(1)
    }
  }

  ngOnInit() {
  }

  reset(){
    this.Auth.playlist_selected = [];
    this.Auth.router_route('collab/votes')
  }

  send_votes(){
    this.loading = true;
    this.Auth.set_votes(this.Auth.playlist_selected)
      .subscribe(reply =>{
        this.Auth.error = {
          errorContent: reply['msg'],
          errorSeverity: "success"
        };
        this.Auth.get_count_votes();
        this.done = true;
        this.loading = false;
      }, error1 => {
        this.loading = false;
        this.Auth.error_handler(error1);
      })
  }

  remove(id){
    this.Auth.playlist_selected.splice(this.find(id), 1);
    if (!this.Auth.playlist_selected.length){
      // Retour à l'étape 1
      this.Auth.router_step(1)
    }
  }

  find(id){
    return this.Auth.playlist_selected.findIndex(x=> x.id === id)
  }

}
