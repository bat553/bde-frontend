/*
 *
 * bde-frontend
 * summary-votes.component.ts
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-summary-votes',
  templateUrl: './summary-votes.component.html',
  styleUrls: ['./summary-votes.component.scss']
})
export class SummaryVotesComponent implements OnInit {

  tracks_list = [];
  loading : boolean;
  constructor(public Auth : AuthService) {}

  ngOnInit() {
    this.get_votes();
  }

  get_track_infos(id){
    this.Auth.get_track(id)
      .subscribe(reply =>{
        this.tracks_list.push(reply['results']);
        if (this.tracks_list.length === this.Auth.count_votes){
          this.loading = false;
        }
      }, error1 => {
        this.Auth.error_handler(error1);
        this.loading = false;
      })
  }

  get_votes(){
    this.loading = true;
    this.tracks_list = [];
    this.Auth.get_user_votes()
      .subscribe(reply =>{
        reply['votes'].forEach((vote)=>{
          this.get_track_infos(vote['id_track']);
        });
      }, error1 => {
        if (error1['status'] !== 404){
          this.Auth.error_handler(error1)
        }
      })
  }

  delete_vote(id) {
    if (confirm("Êtes-vous sûr de vouloir supprimer ce morceau de votre sélection ?")) {
      this.Auth.delete_vote(id)
        .subscribe(reply =>{
          this.get_votes();
          this.Auth.get_count_votes();
        }, error1 => {
          this.Auth.error_handler(error1)
        })
    }
  }

}
