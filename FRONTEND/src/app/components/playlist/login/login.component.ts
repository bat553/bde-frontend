/*
 *
 * bde-frontend
 * login.component.ts
 *
 * Created by Swano 05/08/19 22:39
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {RecaptchaComponent} from "ng-recaptcha";
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit {
  @ViewChild('captchaRef', {static: false}) captchaRef: RecaptchaComponent;

  login_form = new FormGroup({
    id_inscription : new FormControl(null, [Validators.required, Validators.pattern('[0-9]*')]),
    no_etudiant : new FormControl(null, [Validators.required, Validators.pattern('[0-9]{8}')]),
    "g-recaptcha-response" : new FormControl(null, [Validators.required])
  });

  loading: boolean;
  response: string;

  constructor(public Auth : AuthService) { }

  ngAfterViewInit() {
    this.Auth.get_token((result) => {
      if (result){
        this.Auth.router_step(1)
      }
    })
  }


  getReCaptchaResponse(response: string) {
    if (response){
      this.response = response;
      this.login();
    }
  }


  executeReCaptcha() {
    if (!environment.production){
      /* Si pas en prod bypass recapchat */
      this.login();
    }
    this.captchaRef.execute();
  }


  login(){
    this.login_form.patchValue({
      "g-recaptcha-response": (!environment.production) || this.response
    });
    this.loading = true;
    this.Auth.reset_error();
    this.Auth.playlist_login(this.login_form.value)
      .subscribe(reply =>{
        if (!reply['tos']){
          this.Auth.router_route('collab/first_login')
        }
        this.Auth.user_infos = {
          nom : reply['nom'],
          prenom: reply['prenom'],
          no_etudiant: Number(reply['no_etudiant']),
          titre: reply['titre'],
          id_event: reply['id_event'],
          id_inscription: reply['id_inscription']
        };
        this.Auth.reset_error();
        this.Auth.router_step(1);

        this.loading = false;
        this.captchaRef.reset();
      }, error1 => {
        this.loading = false;
        this.captchaRef.reset();

        this.Auth.error_handler(error1, null, null, 'warning', true, true);
      })
  }


}
