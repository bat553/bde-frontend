/*
 *
 * bde-frontend
 * summary-playlist.component.ts
 *
 * Created by Swano 7/6/19 12:53 AM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-summary-playlist',
  templateUrl: './summary-playlist.component.html',
  styleUrls: ['./summary-playlist.component.scss']
})
export class SummaryPlaylistComponent implements OnInit {

  playlist : object;
  timestamp : string;

  constructor(public Auth : AuthService, private route: ActivatedRoute,) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('id')){
      this.Auth.get_one_playlist(this.route.snapshot.paramMap.get('id'))
        .subscribe(reply =>{
            this.playlist = reply['results'];
            this.timestamp = reply['timestamp'];
            this.Auth.set_title(reply['results']['name'])
        }, error1 => {
          this.Auth.error_handler(error1)
        })
    } else {
      this.Auth.router_route('/playlists');
    }
  }
}
