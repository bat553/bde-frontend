/*
 *
 * bde-frontend
 * step1.component.ts
 *
 * Created by Swano 7/10/19 8:37 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {debounceTime} from 'rxjs/operators';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
  tracks : [];
  loading: boolean;
  timestamp : object;
  help : boolean;

  constructor(public Auth : AuthService) {}

  ngOnInit() {
    this.terms.valueChanges.pipe(
      debounceTime(300)
    ).subscribe(term => {
      if (term && term.length >= 3){
        this.search_track(term);
        this.loading = true;
      } else {
        this.loading = false;
        this.tracks = null;
      }
    });
  }

  terms = new FormControl(null);
  search = new FormGroup({
    terms : this.terms
  });

  search_track(term){
    this.Auth.search_tracks(term)
      .subscribe(reply => {
        this.tracks = reply['results'];
        this.timestamp = reply['timestamp'] || null;
        this.loading = false;
      }, error1 => {
        this.Auth.error_handler(error1);
        this.loading = false;
      })
  }

  select(object){
    this.Auth.playlist_selected.push(object);
  }

  remove(id){
    this.Auth.playlist_selected.splice(this.find(id), 1)
  }

  find(id){
    return this.Auth.playlist_selected.findIndex(x=> x.id === id)
  }

}
