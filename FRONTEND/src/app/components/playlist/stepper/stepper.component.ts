/*
 *
 * bde-frontend
 * stepper.component.ts
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit {
  @Input() step : number;
  @Input() back : boolean;
  @Input() nowarning : boolean;
  constructor(public Auth: AuthService) { }

  ngOnInit() {
    this.Auth.get_token((result)=>{
      if (!result){
        this.Auth.router_route('/collab/login');
      } else if (result === 'tos'){
        this.Auth.router_route('/collab/first_login')
      } else {
        this.Auth.get_count_votes();
      }
    });
  }

}
