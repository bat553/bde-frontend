/*
 *
 * bde-frontend
 * first-login.component.ts
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-first-login',
  templateUrl: './first-login.component.html',
  styleUrls: ['./first-login.component.scss']
})
export class FirstLoginComponent implements OnInit {

  constructor(public Auth: AuthService) { }

  ngOnInit() {
  }

  accept_tos(){
    this.Auth.set_tos()
      .subscribe(reply =>{
        this.Auth.router_step(1)
      }, error1 => {
        this.Auth.error_handler(error1)
      })
  }

}
