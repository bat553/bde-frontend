/*
 *
 * bde-frontend
 * summary-playlists.component.ts
 *
 * Created by Swano 10/08/19 17:31
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-summary-playlists',
  templateUrl: './summary-playlists.component.html',
  styleUrls: ['./summary-playlists.component.scss']
})
export class SummaryPlaylistsComponent implements OnInit {
  @Input() child: boolean;
  timestamp : string;
  playlists = [];
  loading: boolean;

  constructor(public Auth : AuthService) {}

  ngOnInit() {
    this.get_all_playlists();
  }


  get_all_playlists(){
    this.playlists = [];
    this.loading = true;
    this.Auth.get_all_playlists()
      .subscribe(reply =>{
          this.playlists = reply['results'];
          this.timestamp = reply['timestamp'];
          this.loading = false;
      }, error1 => {
        this.Auth.error_handler(error1);
        this.loading = false;
      })
  }

}
