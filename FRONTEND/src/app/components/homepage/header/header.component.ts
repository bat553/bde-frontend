/*
 *
 * bde-frontend
 * header.component.ts
 *
 * Created by Swano 18/08/2019 17:01
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public Auth: AuthService) { }

  ngOnInit() {
    this.get_frontend_urls()
  }

  urls = [];

  get_frontend_urls(){
    this.Auth.get_urls()
      .subscribe(reply =>{
        this.urls = reply['urls']
      }, error => {
        this.Auth.error_handler(error)
      })
  }

}
