/*
 *
 * bde-frontend
 * no-events.component.ts
 *
 * Created by Swano 18/07/19 21:48
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../services/auth.service';

@Component({
  selector: 'app-no-events',
  templateUrl: './no-events.component.html',
  styleUrls: ['./no-events.component.css']
})
export class NoEventsComponent implements OnInit {

  constructor(public Auth : AuthService) { }

  ngOnInit() {
  }

}
