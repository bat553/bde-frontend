/*
 *
 * bde-frontend
 * home.component.ts
 *
 * Created by Swano 6/22/19 8:19 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public Auth: AuthService) { }

  ngOnInit() {
    this.get_upcoming_events();
    this.Auth.reset_error();
  }

  get_upcoming_events(){
    this.Auth.get_upcoming_events()
      .subscribe(reply =>{
        this.Auth.upcoming_events = reply['events']
      }, error1 => {
        this.Auth.upcoming_events = [];
        this.Auth.error_handler(error1)
      })
  }

}
