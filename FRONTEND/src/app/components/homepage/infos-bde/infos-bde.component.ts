/*
 *
 * bde-frontend
 * infos-bde.component.ts
 *
 * Created by Swano 6/22/19 8:19 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-infos-bde',
  templateUrl: './infos-bde.component.html',
  styleUrls: ['./infos-bde.component.css']
})
export class InfosBdeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
