/*
 *
 * bde-frontend
 * hackers.component.ts
 *
 * Created by Swano 28/07/19 19:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hackers',
  templateUrl: './hackers.component.html',
  styleUrls: ['./hackers.component.scss']
})
export class HackersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
