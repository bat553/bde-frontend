/*
 *
 * bde-frontend
 * short-notfound.component.ts
 *
 * Created by Swano 18/08/2019 18:38
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-short-notfound',
  templateUrl: './short-notfound.component.html',
  styleUrls: ['./short-notfound.component.scss']
})
export class ShortNotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
