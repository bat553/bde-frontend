/*
 *
 * bde-frontend
 * events-summary.component.ts
 *
 * Created by Swano 6/25/19 11:52 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-events-summary',
  templateUrl: './events-summary.component.html',
  styleUrls: ['./events-summary.component.scss']
})
export class EventsSummaryComponent implements OnInit {

  constructor(public Auth :AuthService) {}
  next_events = [];
  past_events = [];

  ngOnInit() {
    this.Auth.get_all_events()
      .subscribe(reply =>{
        let now = new Date().getTime();
        reply['events'].forEach((event)=>{
          if (new Date(event['date']).getTime() > now){
            this.next_events.push(event)
          } else {
            this.past_events.push(event)
          }
        });
      }, error1 => {
        this.Auth.error_handler(error1)
      })
  }

}
