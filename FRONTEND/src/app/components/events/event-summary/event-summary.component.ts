/*
 *
 * bde-frontend
 * event-summary.component.ts
 *
 * Created by Swano 04/08/19 18:23
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-event-summary',
  templateUrl: './event-summary.component.html',
  styleUrls: ['./event-summary.component.css']
})
export class EventSummaryComponent implements OnInit {

  id_event : string;
  event_infos : {};
  facebook : boolean;
  cache : boolean;
  updatedAt: string;
  loading: boolean;
  passed : boolean;

  constructor(private route: ActivatedRoute, public Auth: AuthService) { }

  ngOnInit() {
    this.Auth.reset_error();
    this.route.params.subscribe(
      params => {
        this.loading = true;
        this.load(params['id']);
      }
    );
  }

  load(id){
    this.Auth.get_event_facebook(id)
      .subscribe(reply =>{
        this.event_infos = reply['event'];
        this.facebook = reply['facebook'];
        this.cache = reply['cache'];
        this.updatedAt = reply['updatedAt'];
        let now = new Date().getTime();
        let date_event = new Date(reply['event']['local']['date']).getTime();
        this.Auth.set_title(reply['event']['local']['titre']);
        this.Auth.set_meta('description', reply['event']['local']['description']);
        this.Auth.set_meta('og:description', reply['event']['local']['description']);
        if (reply['facebook']){
          /* Set OG img si facebook activé */
          this.Auth.set_meta('og:image', reply['event']['facebook']['cover']['source'])
        }
        this.loading = false;
        this.passed = now > date_event;
      }, error1 => {
        this.loading = false;
        this.Auth.error_handler(error1)
      })
  }
}


