/*
 *
 * bde-frontend
 * app.component.ts
 *
 * Created by Swano 21/08/2019 00:56
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { Component, OnInit } from '@angular/core';
import {AuthService} from "./services/auth.service";
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2Piwik } from 'angulartics2/piwik';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(public Auth : AuthService, private angulartics2Piwik: Angulartics2Piwik){
    angulartics2Piwik.startTracking();
  }

  ngOnInit() {}
}
