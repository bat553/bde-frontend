/*
 *
 * bde-frontend
 * app.module.ts
 *
 * Created by Swano 21/08/2019 13:41
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { BrowserModule } from '@angular/platform-browser';
import {Inject, NgModule, PLATFORM_ID} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MaterialModule} from './material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { library } from '@fortawesome/fontawesome-svg-core';
import {RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings} from 'ng-recaptcha';
import {SEO} from "../environments/seo";

import {faInstagram, faFacebook, faSpotify, faFacebookMessenger, faDiscord} from '@fortawesome/free-brands-svg-icons';
import {
  faSadTear,
  faBars,
  faTimes,
  faStar,
  faCheck,
  faMapMarkedAlt,
  faCalendarDay,
  faArrowRight,
  faExclamationCircle,
  faArrowLeft,
  faHandPointLeft,
  faExternalLinkAlt,
  faPaperPlane,
  faSync,
  faPlusCircle,
  faMinusCircle,
  faQuestionCircle,
  faCheckCircle,
  faHandPointDown,
  faBeer,
  faGlassCheers,
  faUsers,
  faRecycle
} from '@fortawesome/free-solid-svg-icons';
import { Angulartics2Module } from 'angulartics2';

import { AppComponent } from './app.component';
import {AuthService} from './services/auth.service';
import { HomeComponent } from './components/homepage/home/home.component';
import {ServiceWorkerModule, SwPush, SwUpdate} from '@angular/service-worker';
import { environment } from '../environments/environment';
import { FooterComponent } from './components/footer/footer.component';
import { NextEventComponent } from './components/homepage/header/next-event/next-event.component';
import { UpcomingEventsComponent } from './components/homepage/upcoming-events/upcoming-events.component';
import { InfosBdeComponent } from './components/homepage/infos-bde/infos-bde.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { NoEventsComponent } from './components/homepage/header/no-events/no-events.component';
import {TheEyesGuard} from './guard/the-eyes.guard';
import { SidenavComponent } from './components/navigation/sidenav/sidenav.component';
import { ToolbarComponent } from './components/navigation/toolbar/toolbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventSummaryComponent } from './components/events/event-summary/event-summary.component';
import { SummaryLieuComponent } from './components/summary-lieu/summary-lieu.component';
import { ErrorComponent } from './components/error/error.component';
import { EventsSummaryComponent } from './components/events/events-summary/events-summary.component';
import { PageNotfoundComponent } from './components/page-notfound/page-notfound.component';
import { LicenseComponent } from './components/license/license.component';
import { LoginComponent } from './components/playlist/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CustomInterceptor} from './services/http-interceptor.service';
import { SummaryPlaylistsComponent } from './components/playlist/summary-playlists/summary-playlists.component';
import { StepperComponent } from './components/playlist/stepper/stepper.component';
import { Step1Component } from './components/playlist/step1/step1.component';
import { Step2Component } from './components/playlist/step2/step2.component';
import { SummaryPlaylistComponent } from './components/playlist/summary-playlist/summary-playlist.component';
import { SummaryVotesComponent } from './components/playlist/summary-votes/summary-votes.component';
import { HeaderComponent } from './components/homepage/header/header.component';
import { PartenairesComponent } from './components/partenaires/partenaires.component';
import { FirstLoginComponent } from './components/playlist/first-login/first-login.component';
import { HackersComponent } from './components/hackers/hackers.component';
import { ReseauxComponent } from './components/reseaux/reseaux.component';
import {MetaGuard, MetaLoader, MetaModule, MetaStaticLoader, PageTitlePositioning} from '@ngx-meta/core';
import {isPlatformBrowser} from "@angular/common";
import { ShortNotfoundComponent } from './components/short-notfound/short-notfound.component';
import { LegalNoticeComponent } from './components/legal-notice/legal-notice.component';

const appRoutes: Routes = [
  {
    path: '', canActivateChild: [MetaGuard], children: [
      {path: '', component: HomeComponent, canActivate: [TheEyesGuard]},
      {path: 'event/:id', component: EventSummaryComponent, canActivate: [TheEyesGuard]},
      {
        path: 'events', component: EventsSummaryComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Tous les évènements",
            "og:title": "Tous les évènements",
            description: "Tous les évènements organisés par le BDE RT Grenoble.",
            "og:description": "Tous les évènements organisés par le BDE RT Grenoble."
          }
        }
      },
      {path: 'lieu/:id', component: SummaryLieuComponent, canActivate: [TheEyesGuard]},
      {
        path: 'collab/first_login', component: FirstLoginComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Valider les CGU",
            'og:title': "Valider les CGU"
          }
        }
      },
      {
        path: 'bde', component: InfosBdeComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: 'Votre BDE',
            "og:title": 'Votre BDE'
          }
        }
      },
      {
        path: 'playlists', component: SummaryPlaylistsComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Nos playlists",
            "og:title": "Nos playlists",
            description: "Les playlists du BDE RT Grenoble",
            "og:description": "Les playlists du BDE RT Grenoble"
          }
        }
      },
      {path: 'playlist/:id', component: SummaryPlaylistComponent, canActivate: [TheEyesGuard]},
      {
        path: 'collab/login', component: LoginComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Playlists collaborative",
            "og:title": "Playlists collaborative",
            description: "Participez aux playlists des soirées du BDE",
            "og:description": "Participez aux playlists des soirées du BDE"
          }
        }
      },
      {
        path: 'collab', component: LoginComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Playlists collaborative",
            "og:title": "Playlists collaborative",
            description: "Participez aux playlists des soirées du BDE",
            "og:description": "Participez aux playlists des soirées du BDE"
          }
        }
      },
      {
        path: 'collab/step1', component: Step1Component, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Étape 1",
            "og:title": "Étape 1"
          }
        }
      },
      {
        path: 'collab/step2', component: Step2Component, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Étape 2",
            "og:title": "Étape 2"
          }
        }
      },
      {
        path: 'collab/votes', component: SummaryVotesComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Vos votes",
            "og:title": "Vos votes"
          }
        }
      },
      {
        path: 'licenses', component: LicenseComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Licences",
            "og:title": "Licences"
          }
        }
      },
      {
        path: 'reseaux', component: ReseauxComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: 'Les réseaux sociaux',
            description: "Les réseaux sociaux du BDE RT Grenoble",
            'og:title': 'Les réseaux sociaux',
            'og:description': "Les réseaux sociaux du BDE RT Grenoble"
          }
        }
      },
      {
        path: 'legal-notice', component: LegalNoticeComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: 'Mentions légales',
            'og:title': 'Mentions légales',
          }
        }
      },
      {
        path: 'hackers', component: HackersComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: 'sɹǝʞɔɐH',
            description: "CTFà21",
            'og:title': 'sɹǝʞɔɐH',
            'og:description': "CTFà21",
            'og:image': 'https://static.bdertgrenoble.fr/frontend/hackers/hackerman.jpg'
          }
        }
      },
      {
        path: 'partenaires', component: PartenairesComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: 'Les partenaires',
            'og:title': 'Les partenaires',
            'og:description': "Les partenaires du Bureau des étudiants Réseaux et Télécoms de Grenoble",
            description: "Les partenaires du Bureau des étudiants Réseaux et Télécoms de Grenoble"
          }
        }
      },
      {
        path: 'short/404', component: ShortNotfoundComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "URL dynamique introuvable",
            "og:title": "URL dynamique introuvable"
          }
        }
      },
      {
        path: '404', component: PageNotfoundComponent, canActivate: [TheEyesGuard], data: {
          meta: {
            title: "Page introuvable",
            "og:title": "Page introuvable"
          }
        }
      }, /* Routes de redirection (shortcut) */
      {
        path: '**', component: PageNotfoundComponent, canActivate: [TheEyesGuard],
        data: {
          meta: {
            title: "Page introuvable",
            "og:title": "Page introuvable"
          }
        }
      }
    ]
  }
];

export function metaFactory(): MetaLoader {
  return new MetaStaticLoader({
    pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
    pageTitleSeparator: SEO.pageTitleSeparator,
    applicationName: SEO.app_name,
    defaults: {
      title: SEO.title,
      "og:title": SEO.title,
      "og:description": SEO.description,
      description: SEO.description,
      'og:image': SEO.image,
      'og:type': SEO.type,
      'og:url': environment.url_front,
      'og:locale': SEO.local
    }
  });
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    NextEventComponent,
    UpcomingEventsComponent,
    InfosBdeComponent,
    NoEventsComponent,
    SidenavComponent,
    ToolbarComponent,
    EventSummaryComponent,
    SummaryLieuComponent,
    ErrorComponent,
    EventsSummaryComponent,
    PageNotfoundComponent,
    LicenseComponent,
    LoginComponent,
    SummaryPlaylistsComponent,
    StepperComponent,
    Step1Component,
    Step2Component,
    SummaryPlaylistComponent,
    SummaryVotesComponent,
    HeaderComponent,
    PartenairesComponent,
    FirstLoginComponent,
    HackersComponent,
    ReseauxComponent,
    ShortNotfoundComponent,
    LegalNoticeComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule.forRoot(appRoutes,  { scrollPositionRestoration: 'enabled' }),
    MetaModule.forRoot({
      provide: MetaLoader,
      useFactory: (metaFactory)
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    HttpClientModule,
    RecaptchaModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    Angulartics2Module.forRoot(),
  ],
  providers: [AuthService, TheEyesGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: CustomInterceptor,
    multi: true
  }, {provide: RECAPTCHA_SETTINGS, useValue: {siteKey: "6LdNiKAUAAAAAJKjLfYWVa0xSEJnTYDQR3k5gt1s"} as RecaptchaSettings}],
  bootstrap: [AppComponent]
})
export class AppModule {

  isBrowser;
  constructor(update: SwUpdate, snackbar: MatSnackBar, swPush : SwPush, @Inject(PLATFORM_ID) private platformId) {
    this.isBrowser = isPlatformBrowser(platformId);
    library.add(faInstagram, faFacebook,faSadTear, faBars, faTimes, faStar, faCheck, faMapMarkedAlt, faCalendarDay, faArrowRight,
      faExclamationCircle, faArrowLeft, faHandPointLeft, faExternalLinkAlt, faPaperPlane, faSync, faSpotify, faPlusCircle, faMinusCircle, faFacebookMessenger, faQuestionCircle, faCheckCircle,
      faHandPointDown, faBeer, faGlassCheers, faDiscord, faUsers, faRecycle);
    if (this.isBrowser){
      update.available.subscribe(update => {
        // Refresh from the snackbar
        const snack = snackbar.open('Mise à jour disponible.', 'Installer');
        snack
          .onAction()
          .subscribe(() => {
            window.location.reload();
          });
      });
      swPush.notificationClicks.subscribe(notpayload =>{
        console.log(JSON.stringify(notpayload.notification));
        window.open(notpayload.notification.data.url, "_blank");
      });
    }
  };

}
