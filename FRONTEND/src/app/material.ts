/*
 *
 * bde-frontend
 * material.ts
 *
 * Created by Swano 14/07/19 15:21
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';

@NgModule({
  declarations: [],
  imports: [
    CommonModule, MatButtonModule, MatMenuModule, MatSnackBarModule, MatSidenavModule, MatListModule, MatToolbarModule, MatProgressSpinnerModule, MatTooltipModule
  ],
  exports: [
    CommonModule, MatButtonModule, MatMenuModule, MatSnackBarModule, MatSidenavModule, MatListModule, MatToolbarModule, MatProgressSpinnerModule, MatTooltipModule
  ]
})
export class MaterialModule {}
