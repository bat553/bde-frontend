/*
 *
 * bde-frontend
 * auth.service.ts
 *
 * Created by Swano 20/10/2019 13:55
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Location} from '@angular/common';
import {environment} from '../../environments/environment';
import {SEO} from "../../environments/seo";
import { MetaService } from '@ngx-meta/core';
import {SwPush} from '@angular/service-worker';
import {Router} from '@angular/router';

interface Error {
  errorContent: string,
  errorSeverity: string,
  errorUi?: string,
  errorStatus? : number,
  critical?: boolean,
  errorDesc?: string,
  success?:boolean,
  back?: boolean
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private location: Location, private swPush: SwPush, private router : Router, private readonly meta: MetaService) {
    this.get_citation()
  }
  build_infos = [];
  api_front = environment.api_front;
  api_bck = environment.api_bck;
  short_url = environment.short_url;
  url_front = environment.url_front;
  error: Error = null;
  upcoming_events = [];
  sidenav: boolean;
  playlist_selected = [];
  count_votes: number;
  citation = {
    citation : "Chargeeezz!",
    auteur : "Charles de Gaulle",
    date : "22 mars 1976"
  };
  citations = [];
  user_infos = {
    nom: "chargement",
    prenom: "Chargé le",
    no_etudiant: 12121212,
    titre: "",
    id_event: 0,
    id_inscription : 0
  };
  readonly VAPID_PUBLIC_KEY = "BBT5DZ6b5TB4-HiFrBTphDbj6oRWrs_XbPK35bqQcvpAPWVSLRrjxkRz06vi4HlauTSSXzJZs40HhJ8o9W4pcxI";

  get_build_infos(){
    this.build_infos = ["{{XXXX}}", "{{XXXX}}", null];
    this.http.get(this.api_front)
      .subscribe(reply =>{
        this.build_infos =  [reply['commit'], reply['branch'], reply['date']]
      }, error1 => {
        this.error_handler(error1);
      })
  }

  go_back(){
    this.location.back();
  }

  error_desc = {
    404 : "Ce que vous chercher ce trouve dans un autre chateau.",
    500 : "Aie ! Coup dur pour le serveur.",
    512 : "Aie ! Coup dur pour la base de donnée.",
    401 : 'Papers, please...',
    403 : 'Cette porte est verrouillée.'
  };

  error_handler(error : object, errorUI : string = null, msg : string = null, severity : string = null, back: boolean = null, success : boolean = null){
    this.reset_error();
    if (error['status'] == 502 || !error['status']){
      this.error = {
        errorSeverity : "danger",
        errorContent: "Impossible de joindre le serveur.",
        errorDesc: "Merci de vérifier votre connection internet.",
        critical: true
      }
    } else {
      this.error = {
        errorContent: (msg || error['error']['msg'] || null),
        errorSeverity: severity || error['error']['severity'] || "warning",
        errorUi: errorUI || null,
        errorDesc: (this.error_desc[error['status']] || null),
        success: success || error['error']['success'],
        back : back || false
      };
    }
  }

  ask_push(){
    this.swPush.requestSubscription({
      serverPublicKey: this.VAPID_PUBLIC_KEY
    })
      .then(sub => this.addPushSubscriber(sub).subscribe())
      .catch(err => console.log('Alright then, keep your ENDPOINT1!'))
  }

  get_token(cb){
    this.post_token()
      .subscribe(reply =>{
        this.user_infos = {
          no_etudiant : Number(reply['no_etudiant']),
          nom: reply['nom'],
          prenom : reply['prenom'],
          titre: reply['titre'],
          id_inscription : Number(reply['id_inscription']),
          id_event: reply['id_event']
        };
        cb(true)
      }, error1 => {
        cb((error1['error']['tos'] === false) ? 'tos' : false)
      })
  }

  get_count_votes(){
    this.get_user_vote_count()
      .subscribe(reply =>{
        this.count_votes = reply['count'];
        if (reply['count'] >= 10){
          this.router_route('collab/votes')
        }
      }, error1 => {
        this.error_handler(error1)
      })
  }


  post_token(){
    return this.http.get(this.api_front + '/collab/token')
  }

  set_title(newTitle: string = null){
    if (newTitle){
      this.meta.setTitle(newTitle);
      this.meta.setTag('og:title', newTitle)
    } else {
      this.meta.setTitle(SEO.title);
      this.meta.setTag('og:title', SEO.title)
    }
  }

  set_meta(name, content){
    this.meta.setTag(name, content)
  }


  reset_error(){
    this.error = null;
  }

  router_step(id){
    this.router.navigate(['/collab/step'+id])
  }

  router_route(route){
    this.router.navigate([route])
  }

  disconnect(){
    this.http.post(this.api_front + '/collab/disconnect', {})
      .subscribe(reply => {
        this.router.navigate(['/collab/login']);
        this.error = {
          errorContent: "Déconnecté avec succès",
          errorSeverity: "success"
        };
        this.count_votes = null;
        this.playlist_selected = [];
      }, error1 => {
        this.error_handler(error1);
      });

  }

  format_date_heure(date){
    return new Date(date).toLocaleDateString('fr-FR', {timeZone: "Europe/Paris", day:"2-digit",month:"2-digit",year:"2-digit", hour:"2-digit", minute:"2-digit"})
  }

  format_date_event(date){
    return new Date(date).toLocaleDateString('fr-FR', {timeZone: "Europe/Paris", day:"2-digit",month:"long"})
  }

  format_date(date){
    return new Date(date).toLocaleDateString('fr-FR', {timeZone: "Europe/Paris", day:"2-digit",month:"long", year: "numeric"})
  }

  format_date_time_event(date){
    return new Date(date).toLocaleDateString('fr-FR', {timeZone: "Europe/Paris", day:"2-digit",month:"long",hour:"2-digit", minute:"2-digit"})
  }

  format_time_event(date){
    return new Date(date).toLocaleDateString('fr-FR', {timeZone: "Europe/Paris", day:"2-digit",month:"2-digit",year:"2-digit", hour:"2-digit", minute:"2-digit"}).substr(11, 5)
  }

  format_horodatage(date){
    return new Date(date).toLocaleDateString('fr-FR', {timeZone: "Europe/Paris", day:"2-digit",month:"2-digit",year:"2-digit", hour:"2-digit", minute:"2-digit", second: "2-digit", timeZoneName: "short"})
  }

  playlist_login(login){
    return this.http.post(this.api_front + '/collab/login', login)
  }

  get_upcoming_events(){
    return this.http.get(this.api_front + '/upcoming_events')
  }

  get_urls(){
    return this.http.get(this.api_front + '/urls')
  }

  get_a_event(id_event){
    return this.http.get(this.api_front + '/event/' + id_event)
  }

  get_user_votes(){
    return this.http.get(this.api_front + '/collab/votes')
  }

  get_track(id){
    return this.http.get(this.api_front + '/track/'+id)
  }

  get_one_playlist(id){
    return this.http.get(this.api_front + '/playlist/'+id)
  }

  get_all_playlists(){
    return this.http.get(this.api_front + '/playlists')
  }

  search_tracks(terms){
    return this.http.get(this.api_front + '/collab/search/'+terms)
  }

  get_all_events(){
    return this.http.get(this.api_front + '/events')
  }

  get_a_lieu_events(id_lieu){
    return this.http.get(this.api_front + '/lieu/'+id_lieu)
  }

  get_user_vote_count(){
    return this.http.get(this.api_front + '/collab/count/user/votes')
  }

  get_event_facebook(id_event){
    return this.http.get(this.api_front +'/fb/event/'+id_event)
  }

  get_citation(){
    this.citation = {
      citation : "Chargeeezz!",
      auteur : "Charle de Gaulle",
      date : "22 mars 1976"
    };
    if (this.citations.length){
      this.citation = this.citations[Math.floor(Math.random()*this.citations.length)]
    } else {
      this.http.get(this.api_front + '/citations')
        .subscribe(reply => {
          this.citations = this.shuffle(reply['citations']);
          this.citation = this.citations[Math.floor(Math.random()*this.citations.length)]
        }, error => {
          if (error['status'] !== 404){
            this.error_handler(error)
          }
        })
    }
  }

  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }

  set_votes(votes){
    return this.http.post(this.api_front + '/collab/votes', votes)
  }

  set_tos(){
    return this.http.post(this.api_front + '/collab/tos', {})
  }

  delete_vote(id){
    return this.http.request('delete', this.api_front + '/collab/votes', {
      body : {
        id_track: id
      }
    })
  }

  addPushSubscriber(sub){
    return this.http.post(this.api_bck + '/subscribe', sub)
  }
}
