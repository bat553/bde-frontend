/*
 *
 * bde-frontend
 * app.server.module.ts
 *
 * Created by Swano 31/07/19 21:23
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    ModuleMapLoaderModule,
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {}
