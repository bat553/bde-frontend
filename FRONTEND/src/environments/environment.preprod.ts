/*
 *
 * bde-frontend
 * environment.preprod.ts
 *
 * Created by Swano 02/02/2020 17:58
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2020
 *
 */

export const environment = {
  production: true,
  api_bck : "https://api_bck.swla.be",
  url_bck : "https://bde.swla.be",
  api_front : "https://api_front.swla.be",
  url_front: "https://frtnd.swla.be",
  short_url: "https://go.swla.be"
};
