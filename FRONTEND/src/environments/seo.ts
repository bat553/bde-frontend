/*
 *
 * bde-frontend
 * seo.ts
 *
 * Created by Swano 03/08/19 16:25
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

export const SEO = {
  title : 'BDE Réseaux et Télécoms de Grenoble',
  description : "Toutes les infos sur le BDE Réseaux et Télécoms de Grenoble.",
  image: 'https://static.bdertgrenoble.fr/frontend/og-image.jpg',
  app_name: "BDE RT Grenoble",
  type: "website",
  local: "fr_FR",
  title_short: "BDE RT Grenoble",
  pageTitleSeparator: ' - ',
  fbApp_id: "2267060693388869"
};
