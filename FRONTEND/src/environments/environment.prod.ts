/*
 *
 * bde-frontend
 * environment.prod.ts
 *
 * Created by Swano 18/07/19 21:48
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

export const environment = {
  production: true,
  api_bck : "https://api_bck.bdertgrenoble.fr",
  url_bck : "https://panel.bdertgrenoble.fr",
  api_front : "https://api_front.bdertgrenoble.fr",
  url_front: "https://bdertgrenoble.fr",
  short_url: "https://go.bdertgrenoble.fr"
};
