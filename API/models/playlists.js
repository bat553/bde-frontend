/*
 *
 * bde-frontend
 * playlists.js
 *
 * Created by Swano 14/07/19 20:51
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

/*
Ce fichier contient les versions standalones des fonctions playlists (usage interne)
 */

const spotifyApi = require('../config/spotify'),
    SPOTIFY_ACCOUNT = process.env.SPOTIFY_ACCOUNT || 'zcr52bip7qy5jb1hfu0b4pltq' /* Compte spotify bdertgrenoble */,
    Spotify_Cache = require('./spotify').Spotify_Cache,
    Spotify_Token = require('./spotify').Spotify_Token;


exports.search_cache = function (terms, type, cb) {
    // Recherche dans le cache
    if (!terms){
        cb('Bad Request2', null)
    } else {
        Spotify_Cache.findOne({id: ((type === 'playlists') ? SPOTIFY_ACCOUNT : (type === 'casse') ? (String(terms)) : (String(terms)).toLowerCase()), private: false}, (err, cache)=>{
            if (err || !cache){
                cb(null, null)
            } else {
                cb(null, cache)
            }
        })
    }
};


exports.get_token = function(cb){
  Spotify_Token.findOne({user_id: spotifyApi.getClientId(), private: false}, (err, token)=>{
    if (!err && token && token['sp_data']['access_token']){
      cb(null,token['sp_data']['access_token']);
    } else {
      // Renew token
      spotifyApi.clientCredentialsGrant().then(
        function (data) {
          /* Mise en cache pendant 45 min */
          Spotify_Token({
            sp_data: data.body,
            user_id: spotifyApi.getClientId(),
            private: false
          }).save(function (err) {
            if (err && err['code'] !== 11000) {
              console.error(err);
              cb(err, null)
            } else {
              console.log('--- SPOTIFY TOKEN UPDATED ---');
              cb(null, data.body['access_token'])
            }
          });
        },
        function (err) {
          console.error('Spotify : ', err);
          cb("Erreur lors de la connexion avec spotify.", null);
          return false
        }
      )
    }
  })
};

exports.get_track_spotify = function (id_track, access_token, cb) {
    if (!id_track){
        cb('Bad Request3', null)
    } else {
        spotifyApi.setAccessToken(access_token);
        spotifyApi.getTrack(String(id_track), {market: "FR"}, function (err, track) {
            if (err){
                console.error(err);
                cb(err, null)
            } else if (!track){
                cb('Introuvable', null)
            } else {
                cb(null, track.body);
              Spotify_Cache({
                sp_data: track.body,
                id: id_track,
                private: false
              }).save();
            }
        })
    }
};

exports.is_track_valid = function (id_track, cb) {
    /*
    Fonction privé permettant de vérifier si les morceaux soumis par l'utilisateur sont valide
     */
    if (!id_track){
        cb('Bad Request1', null)
    } else {
        // Vérification cache (publique)
        exports.search_cache(id_track, 'casse', function (err, cache) {
            if (err){
                cb(err, null)
            } else if (cache){
                cb(null, cache)
            } else {
                // Demander à spotify
                exports.get_token(function (err, access_token) {
                    if (err){
                        cb(err, null)
                    } else if (!access_token){
                        cb("No access token", null)
                    } else {
                        exports.get_track_spotify(id_track, access_token, function (err, track) {
                            if (err){
                                cb(err, null)
                            } else if (!track){
                                cb("Introuvable", null)
                            } else {
                                cb(null, track)
                            }
                        })
                    }
                })
            }
        })
    }
};
