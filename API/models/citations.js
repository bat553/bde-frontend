/*
 *
 * bde-frontend
 * citations.js
 *
 * Created by Swano 7/3/19 6:24 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


const sql = require('../config/db');

let Citation = function (citation) {
  this.citation = citation.citation;
  this.auteur = citation.auteur;
  this.date = citation.date;
};

Citation.getAll = function(result){
  sql.query("SELECT citation, auteur, date FROM citations_elite WHERE public = 1", function (err, res) {
    if (err){
      result(err.code, null)
    } else {
      result(null, res)
    }
  })
};

module.exports = Citation;
