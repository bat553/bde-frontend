/*
 *
 * bde-frontend
 * lieux.js
 *
 * Created by Swano 7/6/19 6:22 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const sql = require('../config/db');

let Lieu = function (lieu) {
  this.id_lieu = lieu.id_lieu;
  this.nom = lieu.nom;
  this.capacitee = lieu.capacitee;
  this.adresse = lieu.adresse;
  this.id_responsable = lieu.id_responsable;
  this.gmap = lieu.gmap;
};


Lieu.getOne = function(id_lieu, result){
  sql.query("SELECT evenements.id_event, evenements.titre, evenements.date FROM evenements WHERE id_lieu = ? AND public = 1 ORDER BY date DESC LIMIT 5", String(id_lieu), function (err, events) {
    if (err){
      console.error(err);
      result(err.code, null, null)
    } else if (!events || !events.length){
      result(null, null, null)
    } else {
      sql.query("SELECT nom, id_lieu, adresse, gmap FROM lieux WHERE id_lieu = ?", String(id_lieu), function (err, lieu) {
          if (err){
            console.error(err);
            result(err.code, null, null)
          } else {
            result(null, lieu[0], events)
          }
      })
    }
  })
};

module.exports = Lieu;
