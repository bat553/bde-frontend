/*
 *
 * bde-frontend
 * spotify.js
 *
 * Created by Swano 10/08/19 17:59
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/*
  Ce module cache les clients token de spotify pendant 45 min
 */

const Spotify_Token = new Schema({
    sp_data : {
        type: Object,
        required : true
    },
    user_id : {
        type: String,
        required: true
    },
	private : { /* Si le cache ne doit être utilisé que par le backoffice */
		type: Boolean,
		required: true,
		default: false
	},
    timestamp : {
        type: Date,
        required: true,
        default : function () {
            return new Date()
        }
    }
});


const Spotify_Cache = new Schema({
  sp_data : {
    type: Object,
    required : true
  },
  id : {
    type: String,
    required: true
  },
  private : { /* Si le cache ne doit être utilisé que par le backoffice */
	type: Boolean,
	required: true,
	default: false
  },
  timestamp : {
    type: Date,
    required: true,
    default : function () {
      return new Date()
    }
  }
});

const Spotify_Vote = new Schema({
    id_track: {
        type: String,
        required: true
    },
    id_event: {
        type: Number,
        required: true
    },
    id_votant : {
        type: String,
        required: true
    },
    removed : {
      type: Boolean,
      default: false,
      required: true
    },
    timestamp: {
        type: Date,
        required: true,
        default: function () {
            return new Date()
        }
    }
});

const Spotify_Clients_Token = new Schema({
  refresh_token : {
    type: String,
    required : true
  },
  user_id : {
    type: String,
    required: true
  },
  timestamp : {
    type: Date,
    required: true,
    default : function () {
      return new Date()
    }
  }
});

const Vote = function(vote, req){
  this.id_votant = req.session.infos['id_inscription'];
  this.id_event = req.session.infos['id_event'];
  this.id_track = String(vote);
};

Spotify_Token.index({timestamp: 1}, {expireAfterSeconds : 2700}); // 45 minutes
Spotify_Cache.index({timestamp: 1}, {expireAfterSeconds : 10800}); // 3 heures
Spotify_Vote.index({id_track: 1, id_votant : 1}, {unique : true}); // Voter une fois pour une chanson

exports.Vote = Vote;
exports.Spotify_Clients_Token = mongoose.model("Spotify_Clients_Token", Spotify_Clients_Token, "spotify_clients_tokens");
exports.Spotify_Vote = mongoose.model("Spotify_Vote", Spotify_Vote, 'spotify_votes');
exports.Spotify_Token = mongoose.model("Spotify_Token", Spotify_Token, "spotify_token");
exports.Spotify_Cache = mongoose.model("Spotify_Cache", Spotify_Cache, "spotify_cache");
