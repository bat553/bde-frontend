/*
 *
 * bde-frontend
 * short_urls.js
 *
 * Created by Swano 26/08/2019 16:37
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const sql = require('../config/db');

let URL = function (url) {
  this.original = url.original;
  this.short = url.short;
};

URL.getOne = function(short, result){
  sql.query("SELECT original, id_url FROM short_url WHERE short = ?", String(short), function (err, res) {
    if (err) {
      console.error(err);
      result(err.code, null)
    } else {
      result(null, res[0])
    }
  })
};

URL.getPublics = function(result){
  sql.query("SELECT nom, short, new_window FROM short_url WHERE frontend = 1 ORDER BY timestamp DESC LIMIT 5", function (err, res) {
    if (err) {
      console.error(err);
      result(err.code, null)
    } else {
      result(null, res)
    }
  })
};

module.exports = URL;
