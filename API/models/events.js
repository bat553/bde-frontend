/*
 *
 * bde-frontend
 * events.js
 *
 * Created by Swano 02/02/2020 22:18
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2020
 *
 */


const sql = require('../config/db');
let Evenement = function () {};

Evenement.getUpcoming = function(result){
  sql.query("SELECT  evenements.id_event, evenements.titre, evenements.linked_to, evenements.description, evenements.prix, evenements.preinscription, evenements.id_lieu, evenements.date, evenements.fb_id, lieux.nom as 'nom_lieu', evenements.playlist, evenements.spotify_id FROM evenements INNER JOIN lieux ON evenements.id_lieu = lieux.id_lieu WHERE date >= CURDATE() AND public = 1 AND linked_to IS NULL ORDER BY evenements.date LIMIT 5", function (err, res) {
    if (err){
      console.error(err);
      result(err.code, null)
    } else {
      result(null, res)
    }
  })
};

Evenement.getOne = function(id_event, result){
  sql.query("SELECT evenements.id_event, evenements.titre, evenements.linked_to, evenements.description, evenements.prix, evenements.public, evenements.preinscription, evenements.id_lieu, evenements.date, evenements.fb_id, lieux.nom as 'nom_lieu', lieux.gmap as 'gmap', evenements.playlist, evenements.spotify_id FROM evenements INNER JOIN lieux ON evenements.id_lieu = lieux.id_lieu WHERE public = 1 AND id_event = ?", String(id_event), function (err, event){
    if (err){
      console.error(err);
      result(err.code, null)
    } else {
      result(null, event[0])
    }
  })
};

Evenement.getSlaveInfos = function(id_event, result){
  sql.query("SELECT evenements.id_event, evenements.titre, evenements.linked_to, evenements.description, evenements.prix, evenements.public, evenements.preinscription, evenements.id_lieu, evenements.date, evenements.fb_id, lieux.nom as 'nom_lieu', lieux.gmap as 'gmap', evenements.playlist, evenements.spotify_id FROM evenements INNER JOIN lieux ON evenements.id_lieu = lieux.id_lieu WHERE id_event = ?", Number(id_event), function (err, event){
    if (err){
      console.error(err);
      result(err.code, null)
    } else if (!event.length || (!event[0]['linked_to'] && event[0]['public'] !== 1)) {
      result(null, null, null) // Introuvable
    } else if (!event[0]['linked_to']){
      // Verification si il y a des évènements liés
      sql.query("SELECT id_event, titre, prix, preinscription FROM evenements WHERE linked_to = ?", Number(event[0]['id_event']), function (err, linked) {
        if (err){
          console.error(err);
          result(err.code, null, null)
        } else {
          result(null, event[0], linked) // Si pas lié à un évènement
        }
      })
    } else {
      // Si lié à un évènement
      sql.query("SELECT evenements.id_event, evenements.titre, evenements.linked_to, evenements.description, evenements.prix, evenements.public, evenements.preinscription, evenements.id_lieu, evenements.date, evenements.fb_id, lieux.nom as 'nom_lieu', lieux.gmap as 'gmap', evenements.playlist, evenements.spotify_id FROM evenements INNER JOIN lieux ON evenements.id_lieu = lieux.id_lieu WHERE public = 1 AND id_event = ?", String(event[0]['linked_to']), function (err, master){
        if (err){
          console.error(err);
          result(err.code, null, null)
        } else if (!master.length){
          result(null, null, null) // Maitre Introuvable
        } else {
          // Modifier les infos
          result(null, {
            titre: event[0]['titre'],
            description: event[0]['description'] || master[0]['description'],
            id_lieu: event[0]['id_lieu'],
            date: event[0]['date'],
            fb_id: master[0]['fb_id'],
            nom_lieu: event[0]['nom_lieu'],
            gmap: event[0]['gmap'] || null,
            spotify_id: master[0]['spotify_id'],
            playlist: master[0]['playlist'],
            preinscription: event[0]['preinscription'],
            prix: event[0]['prix'],
            linked_to: event[0]['linked_to']
          }, null)
        }
      })
    }
  })
};

Evenement.getOnePlaylist = function(id_event, result){
  sql.query("SELECT evenements.playlist, evenements.public, evenements.titre, evenements.spotify_id FROM evenements WHERE public = 1 AND  playlist = 1 AND id_event = ?", String(id_event), function (err, res) {
    if (err){
      console.error(err);
      result(err.code, null)
    } else {
      result(null, res[0])
    }
  })
};

Evenement.getPublished = function(result){
  sql.query("SELECT evenements.id_event, evenements.titre, evenements.linked_to, evenements.description, evenements.prix, evenements.preinscription, evenements.id_lieu, evenements.date, evenements.fb_id, lieux.nom as 'nom_lieu', lieux.gmap as 'gmap', evenements.playlist, evenements.spotify_id FROM evenements INNER JOIN lieux ON evenements.id_lieu = lieux.id_lieu WHERE public = 1 AND linked_to IS NULL ORDER BY evenements.date DESC", function (err, res) {
    if (err){
      console.error(err);
      result(err.code, null)
    } else {
      result(null, res)
    }
  })
};

module.exports = Evenement;
