/*
 *
 * bde-frontend
 * short_urls_tracker.js
 *
 * Created by Swano 28/07/19 22:06
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/*
  Ce module enregistre les cliques sur les liens
 */

const URL_Tracker = new Schema({
  id_url : {
    type: Number,
    required: false
  },
  short : {
    type: String,
    required: true
  },
  query: {
    type: Object
  },
  timestamp : {
    type: Date,
    required: true,
    default : function () {
      return new Date()
    }
  }
});

exports.URL_Tracker = mongoose.model("URL_Tracker", URL_Tracker, "url_tracker");
