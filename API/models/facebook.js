/*
 *
 * bde-frontend
 * facebook.js
 *
 * Created by Swano 6/23/19 10:49 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/*
Ce module permet de garder en caches les infos de facebook pendant un temps donnée afin d'éviter de query l'API tout le temps.
 */

const FB_Cache = new Schema({
  fb_data : {
    type: Object,
    required : true
  },
  timestamp : {
    type: Date,
    required: true,
    default : function () {
      return new Date()
    }
  },
  fb_id: {
    type: Number,
    required: true
  }
});

FB_Cache.index({timestamp: 1}, {expireAfterSeconds : 1800}); // 30 minutes

exports.FB_Cache = mongoose.model("FB_Cache", FB_Cache, "fb_cache");
