/*
 *
 * bde-frontend
 * inscriptions.js
 *
 * Created by Swano 13/09/2019 14:24
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


let sql = require('../config/db');

let Inscription = function () {};

Inscription.Login = function(id_inscription, no_etudiant, result){
  sql.query("SELECT inscriptions.*, e.linked_to, e.titre FROM inscriptions INNER JOIN evenements e on inscriptions.id_event = e.id_event WHERE e.playlist = 1 AND inscriptions.id_inscription = ? AND inscriptions.no_etudiant = ? AND e.date > NOW()", [Number(id_inscription), String(no_etudiant)], function (err, res) {
    if (err){
      result(err.code, null)
    } else {
      result(null, res[0])
    }
  })
};

Inscription.getOneNoEtu = function(id_inscription, no_etudiant, result){
  sql.query("SELECT inscriptions.*, e.linked_to, e.titre, e.spotify_id FROM inscriptions INNER JOIN evenements e on inscriptions.id_event = e.id_event WHERE inscriptions.id_inscription = ? AND inscriptions.no_etudiant = ?", [Number(id_inscription), String(no_etudiant)], function (err, res) {
    if (err){
      result(err.code, null)
    } else {
      result(null, res[0])
    }
  })
};

Inscription.setTos = function(id_inscription, no_etudiant, tos, result){
  sql.query("UPDATE inscriptions SET tos = ? WHERE id_inscription = ? AND no_etudiant = ?", [tos, Number(id_inscription), String(no_etudiant)], function (err, res) {
    if (err){
      result(err.code, null)
    } else {
      result(null, res)
    }
  })
};

module.exports = Inscription;
