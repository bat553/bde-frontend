/*
 *
 * bde-frontend
 * tos_ack.js
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


/*
Model mongoDB pour valider qui à accepté les "Terms of Services"
 */


const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const TOS_ACK = new Schema({
  timestamp: {
    type: Date,
    required: true,
    default: function () {
      return new Date();
    }
  },
  id_inscription : {
    type: Number,
    required : true
  },
  no_etudiant : {
    type: Number,
    required: true
  }
});


TOS_ACK.index({no_etudiant: 1, id_inscription : 1}, {unique : true});
exports.TOS_ACK = mongoose.model("TOS_ACK", TOS_ACK, 'tos_ack');
