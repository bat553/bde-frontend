/*
 *
 * bde-frontend
 * playlist.js
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


module.exports = function (app) {
  const playlistController = require('../controllers/playlist'),
    rateLimiter = require('../config/rate-limit');

  app.route('/collab/search/:terms?')
    .get(rateLimiter.low_cpu, playlistController.is_Auth, playlistController.search_cache, playlistController.get_token, playlistController.search_spotify);

  app.route('/collab/login')
    .post(rateLimiter.medium_cpu, playlistController.valid_recaptcha, playlistController.login_user);

  app.route('/collab/token')
    .get(rateLimiter.low_cpu, playlistController.is_Auth, playlistController.token);

  app.route('/collab/disconnect')
    .post(rateLimiter.low_cpu, playlistController.disconnect);

  app.route('/playlists')
    .get(rateLimiter.low_cpu, (req, res, next) =>{res.playlists = true; next() },playlistController.search_cache, playlistController.get_token, playlistController.get_playlists_spotify);

  app.route('/playlist/:terms')
    .get(rateLimiter.low_cpu, (req, res, next) =>{res.casse = true; next() }, playlistController.search_cache, playlistController.get_token, playlistController.get_playlist_spotify);

  app.route('/track/:terms')
    .get(rateLimiter.low_cpu, playlistController.is_Auth,(req, res, next) =>{res.casse = true; next() }, playlistController.search_cache, playlistController.get_token, playlistController.get_track_spotify);

  app.route('/collab/votes')
    .get(rateLimiter.low_cpu, playlistController.is_Auth, playlistController.get_user_votes)
    .delete(rateLimiter.medium_cpu, playlistController.is_Auth, playlistController.delete_vote)
    .post(rateLimiter.high_cpu, playlistController.is_Auth, playlistController.vote);

  app.route('/collab/count/user/votes')
    .get(rateLimiter.low_cpu, playlistController.is_Auth, (req, res, next)=>{res.count = true; next()}, playlistController.get_user_votes);

  app.route('/collab/tos')
    .post(rateLimiter.medium_cpu, (req, res, next)=>{res.tos = true; next()}, playlistController.is_Auth, playlistController.set_tos);
};
