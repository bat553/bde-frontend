/*
 *
 * bde-frontend
 * citations.js
 *
 * Created by Swano 20/10/2019 13:55
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

module.exports = function (app) {
  let CitationController = require('../controllers/citations'),
    rateLimiter = require('../config/rate-limit');


  app.route('/citation')
    .get(rateLimiter.low_cpu, CitationController.get_random_citation);

  app.route('/citations')
    .get(rateLimiter.medium_cpu, CitationController.get_publics_citations)

};
