/*
 *
 * bde-frontend
 * events.js
 *
 * Created by Swano 15/09/2019 22:21
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


module.exports = function (app) {
  let eventController = require('../controllers/events'),
    rateLimiter = require('../config/rate-limit');

  app.route('/upcoming_events')
    .get(rateLimiter.low_cpu, eventController.get_upcoming_events);

  app.route('/event/:id_event')
    .get(rateLimiter.low_cpu, eventController.get_one_event);

  app.route('/events')
    .get(rateLimiter.low_cpu, eventController.get_all_events);

  app.route('/fb/event/:id_event')
    .get(rateLimiter.low_cpu, eventController.get_event_fb);

  app.route('/lieu/:id_lieu')
    .get(rateLimiter.low_cpu, eventController.get_lieu_events)

};
