/*
 *
 * bde-frontend
 * rate-limit.js
 *
 * Created by Swano 28/07/19 17:22
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const RateLimit = require('express-rate-limit'),
  MONGO_ADDRESS = process.env.MONGO_ADDRESS || "mongodb",
  MongoStore = require('rate-limit-mongo');

const error_message = {success: false, msg: 'Trop de requetes depuis cette IP, merci de réessayer dans quelques instants'};

exports.low_cpu = new RateLimit({
  /* Oppérations avec faible coût CPU (Global)  */
  // 300 requetes pour 10 min
  store: new MongoStore({
    uri: "mongodb://"+MONGO_ADDRESS+":27017/BDE_BCO",
    collectionName: "rate_limiting"
  }),
  max : 300,
  windowMs : 10 * 60 * 1000,
  skipFailedRequests: true,
  expireTimeMs: 10 * 60 * 1000,
  message: error_message
});


exports.medium_cpu = new RateLimit({
  /* Oppération avec un coût CPU moyen (Login/reset) */
  // 150 requetes pour 10 min
  store: new MongoStore({
    uri: "mongodb://"+MONGO_ADDRESS+":27017/BDE_BCO",
    collectionName: "rate_limiting"
  }),
  max : 150,
  windowMs : 10 * 60 * 1000,
  expireTimeMs: 10 * 60 * 1000,
  message: error_message
});

exports.high_cpu = new RateLimit({
  /* Oppération avec un coût CPU haut (Votes/Ajouts dans la bdd) */
  // 75 requetes pour 10 min
  store: new MongoStore({
    uri: "mongodb://"+MONGO_ADDRESS+":27017/BDE_BCO",
    collectionName: "rate_limiting"
  }),
  max : 75,
  windowMs : 10 * 60 * 1000,
  expireTimeMs: 10 * 60 * 1000,
  message: error_message
});
