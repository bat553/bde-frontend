/*
 *
 * bde-frontend
 * spotify.js
 *
 * Created by Swano 7/3/19 11:31 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


const SpotifyWebApi = require('spotify-web-api-node');

const spotifyApi = new SpotifyWebApi({
  clientId: "ac96afabcbd14b3394f9c66bbd0efb02",
  clientSecret : "9c764af9c9f245e4966323eecee798c9"
});


module.exports = spotifyApi;
