/*
 *
 * bde-frontend
 * check_env.js
 *
 * Created by Swano 02/02/2020 22:18
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2020
 *
 */

/*
Initialisation des variables avant de démarrer le programme
 */

async function InitBDEbco() {
  process.stdout.write("Chargement des variables... ");
  process.env.DEBUG ? console.log(process.env) : null;
  let env = process.env;
  if (!env.MYSQL_HOST || !env.MYSQL_USER || !env.MYSQL_PASSWORD){
    process.stdout.write("Échec !\n");
    throw new Error('Manque un ou plusieurs paramètres obligatoires (DDB)')
  } else if (!env.MONGO_ADDRESS){
    process.stdout.write("Échec !\n");
    throw new Error('Manque un ou plusieurs paramètres obligatoires (MongoDB)')
  } else if (!env.PORT || !env.SESS_NAME || !env.SESS_DOMAIN || !env.COMMIT || !env.DATE || !env.BRANCH || !env.SESS_SECRET || !env.FRONTEND_URL){
    process.stdout.write("Échec !\n");
    throw new Error('Manque un ou plusieurs paramètres obligatoires (Général)')
  } else if (!env.CAPCHA_PUB_KEY || !env.CAPCHA_PRIV_KEY){
    process.stdout.write("Échec !\n");
    throw new Error('Manque un ou plusieurs paramètres obligatoires (reCAPCHA)')
  } else if (!env.FB_ACCESS_TOKEN || !env.FB_VERSION){
    process.stdout.write("Échec !\n");
    throw new Error('Manque un ou plusieurs paramètres obligatoires (Facebook)')
  } else if (!env.LOGS_TOKEN && env.LOGS_ENABLE === "1"){
    process.stdout.write("Échec !\n");
    throw new Error('Manque un ou plusieurs paramètres obligatoires (Logs Token)')
  } else {
    process.stdout.write("Succès !\n");
  }
}

module.exports = InitBDEbco();
