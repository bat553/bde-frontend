/*
 *
 * bde-frontend
 * logs.js
 *
 * Created by Swano 20/10/2019 13:55
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const expressWinston = require('express-winston'),
  winston = require('winston'),
  WinstonGraylog2  = require('winston-graylog2'),
  BRANCH = process.env.BRANCH || "dev",
  COMMIT = process.env.COMMIT || "dev";

const options = {
  name: "BDE RT Grenoble",
  level: 'info',
  graylog: {
    servers: [
      {
        host: 'gra1.logs.ovh.com',
        port: 2202,
      }
    ],
    facility: "FRONTEND"
  },
  handleExceptions: true,
  staticMeta: {
    "X-OVH-TOKEN": process.env.LOGS_TOKEN,
    branch: BRANCH,
    commit: COMMIT,
  }
};

exports.Middleware = expressWinston.logger({
  format: winston.format.json(),
  transports: [
    (COMMIT !== 'dev' && process.env.LOGS_ENABLE) ? new WinstonGraylog2(options) : new winston.transports.Console()
  ],
  statusLevels: true
});

exports.Console = winston.createLogger({
  format: winston.format.json(),
  transports: [
    (COMMIT !== 'dev' && process.env.LOGS_ENABLE) ? new WinstonGraylog2(options) : new winston.transports.Console()
  ],
  statusLevels: true
});
