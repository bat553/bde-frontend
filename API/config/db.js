/*
 *
 * bde-frontend
 * db.js
 *
 * Created by Swano 7/2/19 6:27 PM
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


const mysql = require('mysql'),
  mysql_root = mysql.createPool({
    host     : process.env.MYSQL_HOST || 'mariadb',
    user     :  process.env.MYSQL_USER || 'admin',
    password :  process.env.MYSQL_PASSWORD || 'bKBlVzDs5WI1dPcBQUMaJXRQuiiQjnWEOlaJ6lbdbCT9JD42nlOLUOdKoW6wzg32tr6Cqg5TFLp6Y01BTK',
    port : process.env.MYSQL_PORT || "3306",
    database: 'bde-bco',
    charset : 'utf8mb4'
  });

module.exports = mysql_root;
