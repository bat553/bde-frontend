/*
 *
 * bde-frontend
 * app.js
 *
 * Created by Swano 02/02/2020 17:58
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2020
 *
 */

/* -------- CHECK ENV -------- */
require('./config/check_env').catch((error)=>{
  console.log('\n',error);
  process.exit(2);
}).then(()=>{
  const morgan = require('morgan'),
    express = require('express'),
    bodyParser = require('body-parser'),
    expressSession = require('express-session'),
    MongoStore = require('connect-mongo')(expressSession),
    mongoose = require('mongoose'),
    router = express.Router(),
    winstonConsole = require('./config/logs').Console,
    cors = require('cors'),
    helmet = require('helmet'),
    subdomain = require('express-subdomain'),
    mysql_root = require('./config/db'),
    app = express(),
    port = process.env.PORT,
    COMMIT = process.env.COMMIT,
    DATE = process.env.DATE,
    BRANCH = process.env.BRANCH,
    MONGO_ADDRESS = process.env.MONGO_ADDRESS,
    SESS_DOMAIN = process.env.SESS_DOMAIN,
    SESS_SECRET = process.env.SESS_SECRET;

  /* -------- TIMEZONE -------- */
  process.env.TZ = process.env.TZ || "Europe/Paris";

  /* -------- PARAMETERS -------- */
  app.use(bodyParser.json({extended: true, limit: '5mb'}));
  app.use(bodyParser.urlencoded({extended: true, limit: '5mb'}));

  /* -------- PROXY -------- */
  app.set('trust proxy', 'uniquelocal');


  /* -------- LOGS -------- */
  app.use(require('./config/logs').Middleware);

  /* -------- HEADERS SECURITY -------- */
  app.use(helmet());
  app.disable('x-powered-by');

  /* -------- CONSOLE LOGS -------- */
  app.use(morgan('dev'));

  /* -------- CORS -------- */
  app.use(cors({
    methods: "GET,POST,DELETE",
    maxAge: 86400,
    credentials: true,
    origin: ["https://frtnd.swla.be", "http://localhost:4200", "https://ssr.bdertgrenoble.fr", "https://bdertgrenoble.fr"]
  }));

  /* -------- SESSIONS -------- */
  app.use(expressSession({
    name: "front_sid",
    saveUninitialized: false,
    resave: false,
    secret: SESS_SECRET,
    store: new MongoStore({
      url: "mongodb://"+MONGO_ADDRESS+":27017/BDE_BCO",
      collection: "sessions_front",
      stringify: false
    }),
    cookie: {secure: (COMMIT!== 'dev'), httpOnly: (COMMIT!== 'dev'), maxAge: (1000 * 60 * 60 * 24) /* 24 heures */, domain: SESS_DOMAIN}
  }));

  /* -------- BOOTSTRAP -------- */

  /* Connect to MariaDB */
  process.stdout.write("Connexion à MariaDB... ");
  mysql_root.getConnection(function (err) {
    if (err){
      setInterval(()=>{
        return process.exit(1);
      }, 2000);
      process.stdout.write("Échec !\n");
      console.error(err);
      winstonConsole.log('error', 'MariaDB CONNECTION FAILED!!!', err);
    } else {
      process.stdout.write("Succès ! \n");

      /* Connect to MongoDB */
      process.stdout.write("Connexion à MongoDB... ");
      mongoose.set('useNewUrlParser', true);
      mongoose.set('useCreateIndex', true);
      mongoose.connect("mongodb://"+MONGO_ADDRESS+":27017/BDE_BCO", function (err) {
        if (err){
          setInterval(()=>{
            return process.exit(1);
          }, 2000);
          process.stdout.write("Échec !\n");
          console.error(err);
          winstonConsole.log('error', 'MongoDB CONNECTION FAILED!!!', err);
        } else {
          process.stdout.write("Succès ! \n");

          /* INIT */
          console.log("\n-------- API FRONTEND --------\nCOMMIT = " + COMMIT + "\nBRANCH = " + BRANCH);

          /* ROUTES */
          require('./routes/events')(app);
          require('./routes/citations')(app);
          require('./routes/playlist')(app);

          /* URL Shortener */
          app.route('/urls').get(require('./controllers/short_urls').get_urls_frontend);
          router.get('/:short', require('./controllers/short_urls').get_one_url);
          app.use(subdomain('go', router));

          /* -------- Catchers -------- */

          /* Default msg */
          app.get('/', function (req, res) {
            res.status(200).send({success: true, msg: "API FRONTEND", commit: COMMIT, branch: BRANCH, date: DATE})
          });

          /* Not found */
          app.use(function (req, res) {
            res.status(404).send({success: false, msg: "Endpoint not found ¯\\_(ツ)_/¯"})
          });

          /* Uncatched error */
          app.use(function (err, req, res, next) {
            console.error(err);
            res.status(err.statusCode || 500).send({success: false, msg: "Erreur Interne ! CODE : " + err.message || "Inconnue! C'est la merde.", severity: "danger"})
          });

          app.listen(port, function () {
            console.log("\nApplication chargée !");
            console.log("Écoute sur le port : " + port);
            winstonConsole.info('Application chargée !');
            console.log("----------- LOGS -------------")
          });
        }
      })
    }
  });
}).catch((error)=>{
  console.error(error);
  process.exit(2)
});
