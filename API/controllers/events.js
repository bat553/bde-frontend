/*
 *
 * bde-frontend
 * events.js
 *
 * Created by Swano 15/09/2019 22:21
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

let Evenement = require('../models/events'),
  Lieu = require('../models/lieux'),
  winstonConsole = require('../config/logs').Console,
  FB_Cache = require('../models/facebook').FB_Cache,
  request = require('request');

exports.get_upcoming_events = function (req, res) {
  Evenement.getUpcoming(function (err, result) {
    if (err) {
      res.status(512).send({success: false, msg: err})
    } else if (!result || !result.length) {
      res.status(404).send({success: true, msg: "Pas d'évènements à venir enregistré"})
    } else {
      res.status(200).send({success: true, events: result})
    }
  })
};

exports.get_all_events = function(req, res){
  Evenement.getPublished(function (err, result) {
    if (err) {
      res.status(512).send({success: false, msg: err})
    } else if (!result || !result.length) {
      res.status(404).send({success: true, msg: "Pas d'évènements enregistrés"})
    } else {
      res.status(200).send({success: true, events: result})
    }
  })
};

exports.get_lieu_events = function(req, res){
  if (!req.params.id_lieu){
    res.status(400).send({success: false, msg: "Merci de saisir l'id du lieu", champs : ['id_lieu']})
  } else {
    Lieu.getOne(req.params.id_lieu, function (err, lieu, events) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else if (!lieu || !events.length){
        res.status(404).send({success: false, msg: "Lieu introuvable."})
      } else {
        res.status(200).send({success: true, lieu: lieu, events : events})
      }
    })
  }
};

exports.get_one_event = function (req, res) {
  let id_event = Number(req.params['id_event']);
  if (!id_event){
    res.status(400).send({success: false, msg: "Merci de saisir l'id de l'évènement", champs : ['id_event']})
  } else {
    Evenement.getSlaveInfos(id_event, function (err, event, linked) {
      if (err) {
        res.status(512).send({success: false, msg: err})
      } else if (!event) {
        res.status(404).send({success: false, msg: "Évènement introuvable"})
      } else {
        res.status(200).send({success: true, event: event, linked: linked})
      }
    })
  }
};

exports.get_event_fb = function (req, res) {
  let id_event = Number(req.params['id_event']);
  if (!id_event){
    res.status(400).send({success: false, msg: "Merci de saisir l'id de l'évènement", champs : ['id_event']})
  } else {
    Evenement.getSlaveInfos(id_event, function (err, event, linked) {
      if (err) {
        res.status(512).send({success: false, msg: err})
      } else if (!event) {
        res.status(404).send({success: false, msg: "Évènement introuvable"})
      } else if (!event.fb_id || event['linked_to']){
        res.status(200).send({success: true, facebook: false, event :{local: event, linked: linked, facebook: null}})
      } else {
        /* Verification si les donées sont disponibles dans le cache. */
        FB_Cache.findOne({fb_id : event.fb_id}, (err, cache)=>{
          /* Si il est disponible dans le cache, renvoyer la version cached avec le flag. */
          if (err){
            console.error(err);
          } else if (cache){
            res.status(200).send({success: true, facebook: true, cache: true, updatedAt: cache.timestamp, event: {local: event, linked: linked, facebook: cache.fb_data}})
          } else {
            /* Si cache non disponible, refresh sur les API de Facebook et return avec le flag */

            /* Liste des champs à récupérer */
            const fields = "cover,attending_count,description,end_time,interested_count,is_canceled,maybe_count,name," +
              "place,start_time,updated_time";

            // Request
            request('https://graph.facebook.com/'+process.env.FB_VERSION+'/'+event.fb_id+"?fields="+fields+"&access_token="+process.env.FB_ACCESS_TOKEN, {json: true}, (err, response, body)=>{
              if (err) {
                console.error(err);
                res.status(512).send({success: false, msg: "Erreur sur les serveurs de facebook."})
              } else if (body['error']){
                /* Erreur coté FB */
                res.status(512).send({success: false, msg: "Erreur FB", erreur : body['error']})
              } else {
                res.status(200).send({success: true, facebook: true, cache: false, event: {local: event, linked: linked, facebook: body}});
                /* Ajout des données au cache en async */
                let fbcache = new FB_Cache({
                  fb_data : body,
                  fb_id : event.fb_id
                });
                fbcache.save(function (err) {
                  if (err){
                    winstonConsole.error(err);
                  }
                })
              }
            });
          }
        })
      }
    })
  }
};


