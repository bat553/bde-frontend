/*
 *
 * bde-frontend
 * citations.js
 *
 * Created by Swano 20/10/2019 13:55
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

let Citation = require('../models/citations');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

exports.get_random_citation = function (req, res) {
  Citation.getAll(function (err, result) {
    if (err){
      res.status(512).send({success: false, msg: err})
    } else if (!result || !result.length){
      res.status(200).send({success: false, citation : {citation: "En manque d'aspi", auteur : "Roman.", date : "20 decembre 1090"}})
    } else {
      res.status(200).send({success: true, citation: result[getRandomInt(result.length)]})
    }
  })
};

exports.get_publics_citations = function (req, res) {
  Citation.getAll(function (err, citations) {
    if (err){
      res.status(512).send({success: false, msg: err})
    } else if (!citations || !citations.length){
      res.status(200).send({success: false, citations : []})
    } else {
      res.status(200).send({success: true, citations: citations})
    }
  })
};
