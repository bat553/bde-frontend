/*
 *
 * bde-frontend
 * playlist.js
 *
 * Created by Swano 16/09/2019 20:20
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */

const spotifyApi = require('../config/spotify'),
  Inscription = require('../models/inscriptions'),
  Evenement = require('../models/events'),
  SESS_DOMAIN = process.env.SESS_DOMAIN || ".elite21.fr",
  Standalone_playlists_functions = require('../models/playlists'),
  Spotify_Token = require('../models/spotify').Spotify_Token,
  Spotify_Clients_Token = require('../models/spotify').Spotify_Clients_Token,
  Spotify_Vote = require('../models/spotify').Spotify_Vote,
  Vote = require('../models/spotify').Vote,
  TOS_ACK = require('../models/tos_ack').TOS_ACK,
  winstonConsole = require('../config/logs').Console,
  Spotify_Cache = require('../models/spotify').Spotify_Cache;

let SP_USER_ID = '';

const Recaptcha = require('express-recaptcha').RecaptchaV2,
  recaptcha = new Recaptcha(process.env.CAPCHA_PUB_KEY, process.env.CAPCHA_PRIV_KEY);

Spotify_Clients_Token.findOne({}, function (err, sp_acc) {
  if (err || !sp_acc){
    return null
  } else {
    SP_USER_ID = sp_acc['user_id']
  }
});

exports.get_playlists_spotify = function(req, res){
  spotifyApi.setAccessToken(res.locals.sp_token);
  spotifyApi.getUserPlaylists(SP_USER_ID, null, function (err, data) {
    if (err){
      console.error(err);
      res.status(500).send({success: false, msg: "Erreur lors de la connexion à Spotify."})
    } else if (!data || !data.body || !data.body['items'].length){
      res.status(200).send({success: true, results: [], timestamp : false})
    } else {
      res.status(200).send({success: true, results: data.body.items, timestamp : false});
      // Mise en cache
      Spotify_Cache({
        id: SP_USER_ID,
        sp_data: data.body['items'],
		    private: false
      }).save((err)=>{
        if (err){
          winstonConsole.error(err)
        }
      })
    }
  })
};

exports.get_playlist_spotify = function(req, res){
  let terms = String(req.params['terms']);
  if (!terms){
    res.status(400).send({success: false, msg: "Merci de spécifier l'id de la playlist."})
  } else {
    // Check si la playlist est dans un évènement
    spotifyApi.setAccessToken(res.locals.sp_token);
    spotifyApi.getPlaylist(terms, {market: "FR"}, function (err, data) {
      if (err && !err.statusCode) {
        console.error(err);
        res.status(500).send({success: false, msg: "Erreur lors de la connexion à Spotify."})
      } else if (!data || !data.body || (err && err.statusCode)) {
        res.status(err.statusCode || 404).send({success: false, msg: err.message || "Playlist introuvable"})
      } else {
        res.status(200).send({success: true, results: data.body, timestamp: false});
        // Mise en cache
        Spotify_Cache({
          id: terms,
          sp_data: data.body,
		      private: false
        }).save((err)=>{
          if (err){
            winstonConsole.error(err)
          }
        })
      }
    })
  }
};


exports.get_token = function(req, res, next){
  Spotify_Token.findOne({user_id: spotifyApi.getClientId(), private: false}, (err, token)=>{
    if (!err && token && token['sp_data']['access_token']){
      res.locals.sp_token = token['sp_data']['access_token'];
      next();
    } else {
      // Renew token
      spotifyApi.clientCredentialsGrant().then(
        function (data) {
          /* Mise en cache pendant 45 min */
          Spotify_Token({
            sp_data: data.body,
			      user_id: spotifyApi.getClientId(),
			      private: false
          }).save(function (err) {
            if (err && err['code'] !== 11000) {
              console.error(err);
              res.status(512).send({success: false, msg: "Erreur lors de la connexion avec spotify."});
            } else {
              console.log('--- SPOTIFY TOKEN UPDATED ---');
              res.locals.sp_token = data.body['access_token'];
              next();
            }
          });
        },
        function (err) {
          console.error('Spotify : ', err);
          res.status(512).send({success: false, msg: "Erreur lors de la connexion avec spotify."});
          return false
        }
      )
    }
  })
};

exports.search_spotify = function (req, res) {
  let terms = String(req.params['terms']);
  if (!terms){
    res.status(204).send()
  } else {
    spotifyApi.setAccessToken(res.locals.sp_token);
    spotifyApi.searchTracks(terms.toLowerCase(), {limit: 5, market: "FR"}).then(
      function (data) {
        res.status(200).send({success: true, cached: false, results: data.body['tracks']['items']});
        Spotify_Cache({
          sp_data: data.body['tracks']['items'],
          id: terms.toLowerCase(),
		      private: false
        }).save((err)=>{
          if (err){
            winstonConsole.error(err)
          }
        })
      }, function (err) {
        if (err && err['statusCode'] === 404){
          res.status(204).send()
        } else {
          res.status(err['statusCode'] || 512).send({success: false, msg: err['message'] || 'Erreur de connexion avec Spotify', severity: (err['statusCode'] === 400) ? 'warning' : 'danger'})
        }
        console.error(err);
      }
    )
  }
};

exports.valid_recaptcha = function(req, res, next){
  if (!process.env.COMMIT){
    // Si dev : Bypass capchat
    next();
  } else {
    if (!req.body['g-recaptcha-response']){
      res.status(400).send({success: false, msg: "Merci de completer le recaptcha"})
    } else {
      try {
        recaptcha.verify(req, function (err) {
          if (err){
            res.status(400).send({success: false, msg: "Recaptcha invalid " + err})
          } else {
            next();
          }
        })
      } catch (e) {
        res.status(512).send({success: false, msg: "Erreur 500. CODE : recaptcha-verify"})
      }
    }
  }
};

exports.search_cache = function (req, res, next) {
  // Recherche dans le cache avant de demander à spotify
  let terms = String(req.params['terms']);
    Spotify_Cache.findOne({id: ((res.playlists) ? SP_USER_ID : (res.casse)/* Sensible à la casse */ ? terms : terms.toLowerCase()), private: false}, (err, cache)=>{
      if (err || !cache){
        next();
        // Rien trouvé
      } else {
        res.status(200).send({success: true, cached : true, timestamp: cache['timestamp'], results: cache['sp_data']})
      }
    })
};

exports.login_user = function (req, res) {
  let params = {
    no_etudiant: Number(req.body['no_etudiant']),
    id_inscription: Number(req.body['id_inscription'])
  };
  if (!params.no_etudiant || !params.id_inscription){
    res.status(400).send({success: false, msg: "Merci de spécifier votre numéro étudiant et votre id d'inscription", champs: ['no_etudiant', 'id_inscription']})
  } else if (params.no_etudiant === 42042069) {
    res.status(400).send({success: false, msg: "Ce numero etudiant ne peut pas être utilisé."})
  } else {
    Inscription.getOneNoEtu(params.id_inscription, params.no_etudiant, function (err, result) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else if (!result){
        res.status(401).clearCookie("front_sid", {domain: SESS_DOMAIN, path : '/'}).send({success: false, msg: "Inscription introuvable", logout: true});
      } else if (result['disable']){
        res.status(401).send({success: false, msg: "Votre compte à été désactivé. Contactez-nous pour + d'infos"})
      } else if (result['linked_to']) {
        // Si l'évènement est lié à quelque chose
        Evenement.getOnePlaylist(result['linked_to'], (err, linked) => {
          if (err){
            res.status(512).send({success: false, msg: err})
          } else if (!linked){
            res.status(401).clearCookie("front_sid", {domain: SESS_DOMAIN, path : '/'}).send({success: false, msg: "Inscription introuvable", logout: true});
          } else {
           // All good
            req.session.infos = {
              nom : result['nom'],
              prenom : result['prenom'],
              no_etudiant: result['no_etudiant'],
              titre: result['titre'],
              id_event: result['linked_to'],
              id_inscription : result['id_inscription'],
              spotify_id: linked['spotify_id']
            };
            if (!result['tos']){
              res.status(200).send({success: true, tos: false, msg: "Merci d'accepter les conditions d'utilisation du service."})
            } else {
              res.status(200).send({success: true, tos: true, id_event: result['linked_to'], nom: result.nom, prenom: result.prenom, no_etudiant: result.no_etudiant, titre: result['titre'], spotify_id: result.spotify_id})
            }
          }
        })
      } else{
        req.session.infos = result;
        if (!result['tos']){
          res.status(200).send({success: true, tos: false, msg: "Merci d'accepter les conditions d'utilisation du service."})
        } else {
          res.status(200).send({success: true, tos: true, id_event: result.id_event, nom: result.nom, prenom: result.prenom, no_etudiant: result.no_etudiant, titre: result.titre, spotify_id: result.spotify_id})
        }
      }
    })
  }
};

exports.set_tos = function(req, res){
  if (!req.session['infos']['no_etudiant'] || !req.session['infos']['id_inscription']){
    res.status(400).send({success: false, msg: "Merci de spécifier votre numéro étudiant et votre id d'inscription", champs: ['no_etudiant', 'id_inscription']})
  } else {
    Inscription.setTos(req.session['infos']['id_inscription'], req.session['infos']['no_etudiant'], 1, function (err, result) {
      if (err) {
        res.status(512).send({success: false, msg: err})
      } else if (!result || !result.affectedRows) {
        res.status(404).send({success: false, msg: 'Combinaison inscription/no etudiant introuvable'})
      } else {
        res.status(200).send({success: true, msg: "Vous avec accepté les conditions d'utilisations."});
        TOS_ACK({
          id_inscription: Number(req.session['infos']['id_inscription']),
          no_etudiant: Number(req.session['infos']['no_etudiant'])
        }).save((err) => {
          if (err) {
            winstonConsole.error(err);
          }
        })
      }
    })
  }
};


exports.is_Auth = function (req, res, next) {
  if (req.session['infos']) {
    Inscription.getOneNoEtu(req.session['infos']['id_inscription'], req.session['infos']['no_etudiant'], function (err, result) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else if (!result){
        res.status(401).clearCookie("front_sid", {domain: SESS_DOMAIN, path : '/'}).send({success: false, msg: "Inscription introuvable", logout: true});
      } else if (!result['tos'] && !res.tos){
        res.status(401).send({success: false, tos: false, msg: "Merci d'accepter les conditions d'utilisation"})
      } else if(result['disable']){
        res.status(401).send({success: false, msg: "Votre compte à été désactivé. Contactez-nous pour + d'infos"})
      } else if (result['linked_to']) {
        // Si l'évènement est lié à quelque chose
        Evenement.getOnePlaylist(result['linked_to'], (err, linked) => {
          if (err){
            res.status(512).send({success: false, msg: err})
          } else if (!linked){
            res.status(401).send({success: false, msg: "Inscription introuvable", logout: true});
          } else {
            // All good
            req.session.infos = {
              nom : result['nom'],
              prenom : result['prenom'],
              no_etudiant: result['no_etudiant'],
              titre: result['titre'],
              id_event: result['linked_to'],
              id_inscription : result['id_inscription'],
              spotify_id: linked['spotify_id']
            };
            next();
          }
        })
      } else{
        req.session.infos = result;
        next()
      }
    })
  } else {
    res.status(401).send({success: false, msg: "Merci de vous connecter avant de continuer.", logout: true});
  }
};

exports.token = function (req, res) {
  res.send({success: true, id_inscription: req.session.infos['id_inscription'], nom : req.session.infos['nom'], prenom : req.session.infos['prenom'], no_etudiant: req.session.infos['no_etudiant'], titre: req.session.infos['titre'], id_event: req.session.infos['id_event'], spotify_id: req.session.infos['spotify_id']})
};

exports.disconnect = function (req, res) {
  if (req.session){
    req.session.destroy(function (err) {
      if (err) {
        res.status(401).clearCookie("front_sid", {domain: SESS_DOMAIN, path : '/'}).send({success: false, msg: "Token invalide, merci de vous reconnecter."})
      }
      res.status(200).clearCookie("front_sid", {domain: SESS_DOMAIN, path : '/'}).send({success: true})
    })
  } else {
    res.status(401).clearCookie("front_sid", {domain: SESS_DOMAIN, path : '/'}).send({success: false, msg: "Token invalide, merci de vous reconnecter."})
  }
};

create_vote = function (vote, req, cb) {
  const new_vote = new Vote(vote, req);
  if (!new_vote || !new_vote.id_votant || !new_vote.id_track || !new_vote.id_event){
    cb({status: 400, msg: 'no vote', severity: "danger"}, null)
  } else {
    // All good
    Spotify_Vote(new_vote).save((err) => {
      if (err) {
        cb(err, null)
      } else {
        cb(null, true)
      }
    })
  }
};

get_vote_id = function(id_inscription, cb){
  Spotify_Vote.countDocuments({id_votant: Number(id_inscription)}, function (err, count) {
    if (err){
      cb(err, null)
    } else {
      cb(null, count)
    }
  })
};

exports.get_user_votes = function(req, res){
  if (res.count){
    // Si mode count
    this.get_vote_id(req.session.infos['id_inscription'], function (err, count) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else {
        res.status(200).send({success: true, count: count})
      }
    })
  } else {
    // Envoyer la liste des votes
    /* Récupérer la liste sur mongodb */
    Spotify_Vote.find({id_votant: req.session.infos['id_inscription']}, {_id: 0, __v: 0}, function (err, votes) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else if (!votes || !votes.length){
        res.status(404).send({success: false, msg: "Aucun votes trouvés"})
      } else {
        res.status(200).send({success: true, votes: votes})
      }
    })
  }
};

exports.vote = function (req, res) {
  if (!req.body || !Array.isArray(req.body) || req.body.length > 10){
    res.status(400).send({success: false, msg: "Merci de spécifier les votes en format de tableau [] de moins de 10 lignes"})
  } else {
    let votes = req.body;
    /* Check if more than 10 entries */
    this.get_vote_id(req.session.infos['id_inscription'], function (err, count) {
      if (err) {
        res.status(500).send({success: false, msg: err})
      } else if (count >= 10 || votes.length + count > 10) {
        res.status(403).send({success: false, msg: 'Vous ne pouvez pas ajouter plus de 10 morceaux'})
      } else {
        let step1 = 0;
        let step2 = 0;
        let array = [];
        // Verification doublons locaux
        votes.forEach((vote) => {
          // STEP 1: Vérification des entrées
          if (res.headersSent) {
            return false
          } else if (!String(vote['id']) || !vote['id']) {
            res.status(400).send({success: false, msg: "Merci de spécifier un format correct"})
          } else if (array.findIndex(x=>(vote['id']) === x)>=0){
            res.status(400).send({success: false, msg: "Doublon dans votre requete."})
          } else {
            array.push(vote['id']);
            Standalone_playlists_functions.is_track_valid(String(vote['id']), function (err, result) {
              if (res.headersSent) {
                return false
              } else if (err) {
                res.status(err['statusCode'] || 512).send({
                  success: false,
                  msg: err['message'] || 'Erreur inconnue, réessayez plus tard.'
                })
              } else if (!result) {
                res.status(404).send({
                  success: false,
                  msg: 'Titre introuvable (' + vote['id'] + ')',
                  severity: "warning"
                })
              } else {
                // Verification doublons online
                Spotify_Vote.findOne({
                  id_track: vote['id'],
                  id_votant: req.session.infos['id_inscription']
                }, (err, doublon) => {
                  if (res.headersSent) {
                    return false
                  } else if (err) {
                    res.status(512).send({success: false, msg: err})
                  } else if (doublon) {
                    res.status(400).send({
                      success: false,
                      msg: "Vous ne pouvez pas ajouter deux fois un titre \"" + vote['name'] + "\" (doublon)"
                    })
                  } else {
                    step1++;
                    if (step1 === votes.length) {
                      // STEP 2 : Add to database
                      votes.forEach((vote) => {
                        this.create_vote(vote['id'], req, function (err, save) {
                          if (res.headersSent) {
                            return false
                          } else if (err && err['status']) {
                            console.log(err);
                            res.status(err['status']).send({
                              success: false,
                              msg: err['msg'],
                              severity: err['severity']
                            });
                          } else if (err) {
                            console.error(err);
                            res.status(500).send({
                              success: false,
                              msg: err['message'] || 'Erreur inconnue',
                              severity: "danger"
                            })
                          } else {
                            step2++;
                          }
                          if (step2 === votes.length) {
                            res.status(201).send({success: true, msg: "Votes pris en compte"})
                          }
                        })
                      })
                    }
                  }
                })
              }
            })
          }
        })
      }
    })
  }
};


exports.get_track_spotify = function (req, res) {
  let terms = String(req.params['terms']);
  if (!terms){
    res.status(400).send({success: false, msg: "Merci de spécifier l'id du morceau"})
  } else {
    spotifyApi.setAccessToken(res.locals.sp_token);
    spotifyApi.getTrack(terms, {market: "FR"}, function (err, track) {
      if (err){
        res.status(err['statusCode'] || 512).send({success: false, msg: err['message'] || 'Erreur inconnue, réessayez plus tard.'})
      } else if (!track){
        res.status(err.statusCode || 404).send({success: false, msg: err.message || "Morceau introuvable"})
      } else {
        res.status(200).send({success: true, timestamp: false, results : track.body});
        Spotify_Cache({
          sp_data: track.body,
          id: terms
        }).save((err)=>{
          if (err){
            winstonConsole.error(err)
          }
        })
      }
    })
  }
};


exports.delete_vote = function (req, res) {
  let id_track = String(req.body['id_track']);
  if (!id_track) {
    res.status(400).send({success: false, msg: "Merci de spécifier l'id du morceau", champs: ['id_track']})
  } else {
    Spotify_Vote.deleteOne({id_track: id_track, id_votant: req.session.infos['id_inscription']}, function (err, result) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else if (!result || !result['deletedCount']){
       res.status(404).send({success: false, msg: "Morceau introuvable"})
      } else {
        res.status(200).send({success: true, id_track: id_track})
      }
    })
  }
};
