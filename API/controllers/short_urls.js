/*
 *
 * bde-frontend
 * short_urls.js
 *
 * Created by Swano 21/12/2019 18:20
 *
 * Copyright (C) Baptiste PELLARIN - All Rights Reserved
 * 	Unauthorized copying of this file, via any medium is strictly prohibited
 * 	Proprietary and confidential
 * 	Written by Baptiste PELLARIN swano@swano-lab.net, 2019
 *
 */


let URL = require('../models/short_urls'),
  winstonConsole = require('../config/logs').Console,
  URL_Tracker = require('../models/short_urls_tracker').URL_Tracker;

exports.get_one_url = function (req, res) {
  let short = String(req.params['short']);
  if (!short){
    res.status(400).send({success: false, msg: "Merci de spécifier un code", champs : ['short']})
  } else {
    URL.getOne(short, function (err, result) {
      if (err){
        res.status(512).send({success: false, msg: err})
      } else if (!result){
        res.status(307).header('Location', [('https://'+process.env.FRONTEND_URL || 'http://localhost:4200') + '/short/404']).send();
        URL_Tracker({
          short : short,
          query: req.query
        }).save((err)=>{
          if (err){
            winstonConsole.error(err)
          }
        });
      } else {
        res.status(302).header('Location', [result['original']]).send();
        URL_Tracker({
          short : short,
          query: req.query,
          id_url: result['id_url']
        }).save((err)=>{
          if (err){
            winstonConsole.error(err)
          }
        });
      }
    })
  }
};

exports.get_urls_frontend = function (req, res) {
  URL.getPublics(function (err, urls) {
    if (err){
      res.status(512).send({success: false, msg: err})
    } else {
      res.status(200).send({success: true, urls: urls})
    }
  })
};
