#!/usr/bin/env bash
echo "Upload www"
chmod 700 ./config/ssh/cicd_frontend_key
sftp -P 50224 -o StrictHostKeyChecking=no -i ./config/ssh/cicd_frontend_key cicd_frontend@$1 << EOT

lcd dist
cd www
rm browser/assets/*
rmdir browser/assets
rm browser/*
rmdir browser
rm server/*
rmdir server
rm *
put -r *

EOT
